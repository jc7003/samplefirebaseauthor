package com.owen.testfirebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.owen.testfirebase.widget.User;

public class FirebaseDBActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friebase_db);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void commit(View view){
        String name = ((EditText)findViewById(R.id.editText)).getText().toString();
        String email = ((EditText)findViewById(R.id.editText2)).getText().toString();
        String phone = ((EditText)findViewById(R.id.editText3)).getText().toString();

        writeDate(name, email, phone);
    }

    private void writeDate(String name, String email, String phone){
        User user = new User(name, email, phone);
        mDatabase.child("users").setValue(user);

    }
}
