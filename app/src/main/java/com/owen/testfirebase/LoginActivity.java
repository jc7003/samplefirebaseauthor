package com.owen.testfirebase;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.owen.testfirebase.widget.User;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String mUserUID;
    private EditText mEmail, mPwd;
    private Button mLogInBtn;
    private TextView mNumberInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEmail = (EditText)findViewById(R.id.email);
        mPwd = (EditText)findViewById(R.id.password);
        mLogInBtn = (Button)findViewById(R.id.login_button);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener(){

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                mUserUID = null;
                if(user != null) {
                    mUserUID = user.getUid();

                    enableEditText(false);
                    mLogInBtn.setText("登出");
                    mNumberInfo.setText("");
                }
                else{
                    enableEditText(true);
                    mLogInBtn.setText("登入");
                }
            }
         };
        mNumberInfo = (TextView) findViewById(R.id.number_data);
    }

    public void login(View view){

        if(mUserUID != null){
            mAuth.signOut();
            return;
        }

        String email = ((EditText)findViewById(R.id.email)).getText().toString();
        String password = ((EditText)findViewById(R.id.password)).getText().toString();

        login(email, password);
    }

    private void login(String email, String password){
        if(!checkInfo(email, password))
            return;

        Log.d("mAuth", email + "/" + password);

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                String stateText = "帳號或密碼錯誤";
                if(task.isSuccessful()){
                    stateText = "登入成功";

                    initNumberData();
                }

                Toast.makeText(LoginActivity.this, stateText, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initNumberData(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("user");
        myRef.addValueEventListener(new ValueEventListener(){

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren() ){
                    if(ds.getKey().equals(mAuth.getCurrentUser().getUid())){
                        User userInfo = ds.getValue(User.class);
                        StringBuilder br = new StringBuilder();
                        br.append(userInfo.name);
                        br.append("\n");
                        br.append(userInfo.email);
                        br.append("\n");
                        br.append(userInfo.phone);
                        mNumberInfo.setText(br.toString());

                        return;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mNumberInfo.setText("");
            }
        });



    }

    public void registered(View view){
        showSingInDialog();
    }

    private  void showSingInDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View root = LayoutInflater.from(this).inflate(R.layout.singin_layout, null);
        builder.setView(root);
        builder.setTitle("註冊");
        AlertDialog alert = builder.create();
        alert.setButton(DialogInterface.BUTTON_POSITIVE, "確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String name = ((EditText)root.findViewById(R.id.edittext_name)).getText().toString();
                final String email = ((EditText)root.findViewById(R.id.edittext_email)).getText().toString();
                final String pwd = ((EditText)root.findViewById(R.id.edittext_pwd)).getText().toString();
                final String phone = ((EditText)root.findViewById(R.id.edittext_phone)).getText().toString();

                final User userObject = new User(name, email, phone);
                if(!checkInfo(name, email, pwd))
                    return;

                signin(userObject, pwd);

                dialog.dismiss();
            }
        });

        alert.show();
    }

    private void signin(final User user, final String pwd){
        mAuth.createUserWithEmailAndPassword(user.email, pwd).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.d("LoginActivity", "SingInDialog onComplete :" + task.isSuccessful());
                if(task.isSuccessful()){
                    final FirebaseUser users = task.getResult().getUser();
                    if(users != null){
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference myRef = database.getReference("user");
                        myRef.child(users.getUid()).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                login(user.email, pwd);
                            }
                        });
                    }
                }
                else{
                    enableEditText(true);
                }
            }
        });
    }

    private boolean checkInfo(String email, String password){
        if(email.isEmpty() && password.isEmpty()){
            Toast.makeText(LoginActivity.this, "請輸入email/請輸入password", Toast.LENGTH_LONG).show();
            return false;
        }
        if(email.isEmpty()){
            Toast.makeText(LoginActivity.this, "請輸入email", Toast.LENGTH_LONG).show();
            return false;
        }
        if(password.isEmpty()){
            Toast.makeText(LoginActivity.this, "請輸入password", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    private boolean checkInfo(String name, String email, String password){
        StringBuilder error = new StringBuilder();
        if(name.isEmpty())
            error.append("請輸入name");

        if(email.isEmpty()){
            if(error.length() > 0)
                error.append("/");
            error.append("請輸入email");
        }

        if(password.isEmpty()) {
            if (error.length() > 0)
                error.append("/");
            error.append("請輸入password");
        }

        if(error.length() > 0) {
            Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mAuthListener);
    }

    private void enableEditText(boolean state){
        mEmail.setEnabled(state);
        mPwd.setEnabled(state);
    }
}
