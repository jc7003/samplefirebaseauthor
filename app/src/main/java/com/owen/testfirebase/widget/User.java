package com.owen.testfirebase.widget;

/**
 * Created by OwenChen on 2017/3/9.
 */

public class User {

    public String name;
    public String email;
    public String phone;

    public User() {}

    public User(String name, String email, String phone) {
        this.name = name;
        this.email = email;
        this.phone = phone;
    }
}
