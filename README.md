使用firebase的練習

含有登入/登出 與 註冊功能

當註冊/登入成功後便會撈取該會員資料

![螢幕快照 2017-03-15 下午9.58.52.png](https://bitbucket.org/repo/aooqyd/images/1610590830-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202017-03-15%20%E4%B8%8B%E5%8D%889.58.52.png)

註冊畫面如下

![螢幕快照 2017-03-15 下午9.59.57.png](https://bitbucket.org/repo/aooqyd/images/1300259585-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202017-03-15%20%E4%B8%8B%E5%8D%889.59.57.png)